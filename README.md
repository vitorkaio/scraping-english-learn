
## scraping-english-learn

A aplicação passa de página em página do site: [acessar](https://www.mairovergara.com/category/aprendendo-ingles-com-videos/), extrai, captura e salva no mongodb todos os **textos**(inglês, português e vídeo) da página.

---

### Instalando as dependências

    yarn

### Build and Start

A aplicação pode ser executada em modo de **desenvolvimento** atráves do nodemon.

    yarn dev

Pode ser **executado** diretamente com o node. Nesse caso é feito o build primeiramente através do sucrase e depois a execução do mesmo.

    yarn start

Para fazer apenas o **build** da aplicação.

    yarn build
---

### Packages:

 - node
 - mongodb
 - puppeteer
 - sucrase

