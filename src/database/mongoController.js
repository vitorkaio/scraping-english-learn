import Mongo from './mongoInit'

export default async function Items () {
  let client = {}
  let db = {}
  try {
    client = await Mongo
    db = client.getDb()
  } catch (err) {
    throw new Error(err)
  }

  const getIndex = async () => {
    try {
      // const index = await db.collection('index').find({ name: name, lastName: lastName, socialId: socialId })
      const result = await db.collection('info').findOne({})
      return result.index
    } catch (err) {
      throw new Error(err)
    }
  }

  const saveIndex = async (newIndex) => {
    try {
      await db.collection('info').updateOne({}, { $set: { index: newIndex } })
      console.log(`\n*** Index Save: ${newIndex} ***\n`)
    } catch (err) {
      throw new Error(err)
    }
  }

  const saveText = async (data) => {
    try {
      await db.collection('data').insertOne(data)
    } catch (err) {
      throw new Error(err)
    }
  }

  /* const getUsers = async () => {
    try {
      return db.collection('user').find({}).toArray()
    } catch (err) {
      throw new Error(err)
    }
  } */

  const close = () => client.close()

  return {
    getIndex: getIndex,
    saveIndex: saveIndex,
    saveText: saveText,
    close: close
  }
}
