const MongoClient = require('mongodb').MongoClient

const url = 'mongodb://localhost:27017'
const dbName = 'texts'
const mongoClient = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true })

const Mongodb = async () => {
  let client = {}
  let db = {}
  client = await mongoClient.connect()
  db = client.db(dbName)

  const getDb = () => db
  const close = () => client.close()

  return {
    getDb: getDb,
    close: close
  }
}

export default Mongodb()
