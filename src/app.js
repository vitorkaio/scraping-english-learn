import Scrap from './scrap'
import MongoController from './database/mongoController'

const main = async () => {
  const scrap = await Scrap()
  try {
    const size = 15
    const Mongo = await MongoController()
    let index = await Mongo.getIndex()
    console.log(`Index: ${index}`)
    for (; index <= size; index++) {
      const result = await scrap.getInfo(index)
      // scrap.close()
      for (const item of result) {
        console.log(item.title)
        const infoText = await scrap.getInfoTexts(item.link)
        // console.log(infoText)
        await Mongo.saveText({ ...item, ...infoText })
      }
      await Mongo.saveIndex(index + 1)
      // await FireService.saveIndex(index + 1)
    }
    Mongo.close()
  } catch (error) {
    console.log(error)
  } finally {
    scrap.close()
    console.log('Finish')
  }
}

main()
