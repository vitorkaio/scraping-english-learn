import Puppeteer from 'puppeteer'

const Scrap = async () => {
  // const browser = await Puppeteer.launch({ headless: false, devtools: true })
  const browser = await Puppeteer.launch()
  const page = await browser.newPage()

  const goto = async (url) => {
    await page.goto(url)
  }

  // Pega as informações básica, como título e a imagem.
  const getInfo = async (index) => {
    try {
      await goto(`https://www.mairovergara.com/category/aprendendo-ingles-com-videos/page/${index}/`)
      const infos = await page.$$eval('.td-block-row > div > div', infoItems => {
        return infoItems.map((infoItem) => {
          const aref = infoItem.querySelector('.td-module-thumb > a')
          const title = aref.getAttribute('title')
          const link = aref.getAttribute('href')
          const img = infoItem.querySelector('img').getAttribute('src')

          const newItem = { title, link, img }
          return newItem
        })
      })
      return infos
    } catch (error) {
      return error
    }
  }

  // Retorna os texto em inglês e português.
  const getInfoTexts = async (link) => {
    try {
      await goto(link)
      const text = await page.$$eval('.td-post-content > div[style]', items => {
        const newItems = items.filter(item => item.style.width === '50%')
        return newItems.map((item) => {
          item.getAttribute('style')
          const title = item.querySelector('h4 > strong, span').innerText
          const text = item.querySelectorAll('p, p > em')
          const texts = []
          for (const itemText of text) {
            texts.push(itemText.innerText)
          }
          return { title, texts }
        })
      })
      const youtubeLink = await page.evaluate(() => {
        return document.querySelector('.wpb_video_wrapper > iframe').getAttribute('src')
      })
      return { youtubeLink, text }
    } catch (error) {
      return error
    }
  }

  const close = () => {
    browser.close()
  }

  return {
    goto,
    getInfo,
    getInfoTexts,
    close
  }
}

export default Scrap
